# AUTHOR : EFRAYIM FRYDWIN NELSON
# Tugas REST API

Hasil jawaban untuk tugas pengantar Rest Api golang dengan method GET, POST, PUT dan DELETER.

## Setting Database

Project ini menggunakan Database untuk bisa berjalan. Lakukan langkah berikut untuk membuat Database.

```bash
mysql -u root -p
```
```mysql
CREATE DATABASE latihan_restapi;
```
```mysql
USE latihan_restapi;
```
```mysql
CREATE TABLE `mahasiswa` ( 
	`id` Int( 8 ) AUTO_INCREMENT NOT NULL,
	`nim` BigInt( 14 ) NOT NULL,
	`name` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
	`semester` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
	`created_at` DateTime NOT NULL,
	`updated_at` DateTime NOT NULL,
	CONSTRAINT `unique_id` UNIQUE( `id` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 3;
```
## Catatan

Pastikan username dan password pada
```bash
mysql -u [username] -p [password]
```
sesuai dengan server masing-masing.

## Request di Postman

1. Lakukan request method GET pada Postman dengan url:
```url
localhost:7000/mahasiswa
```

2. Methos POST dengan url:
```url
localhost:7000/mahasiswa/create
```

3. Methos PUT dengan url:
```url
localhost:7000/mahasiswa/update
```

4. Methos DELETE dengan url:
```url
localhost:7000/mahasiswa/delete?id=2
```
id=2 adalah contoh untuk mengahapus data dengan id 2.
